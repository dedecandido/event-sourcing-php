<?php

namespace App\Controller;

class BuyerWalletController extends AbstractController
{
    public function __construct();
    public function getWallet($userId);
    public function createWallet(Request $request);
    public function updateWallet(int $userId, Request $request);
    public function suspendWallet(int $userId);
}