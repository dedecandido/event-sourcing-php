<?php

namespace App\Controller;

class BalanceController extends AbstractController
{
    public function __construct();

    /**
     * [getBalance description]
     * @param  [type] $userId [description]
     * @return [type]         [description]
     */
    public function getBalance($userId);
    public function deposit($userId, Request $request);
    public function withdraw($userId, Request $request);
}