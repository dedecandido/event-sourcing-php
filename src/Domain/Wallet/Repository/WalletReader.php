<?php

namespace App\Domain\Wallet\Repository;

interface WalletReader
{
    public function getById(int $id) : Wallet;
}