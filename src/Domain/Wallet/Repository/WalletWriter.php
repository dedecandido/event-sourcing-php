<?php

namespace App\Domain\Wallet\Repository;

interface WalletWriter
{
    public function save();
}