<?php

namespace App\Domain\Wallet\Repository;

class WalletRepository implements WalletReader, WalletWriter
{
    /**
     * [$eventRepository description]
     * @var [type]
     */
    private $eventRepository;

    public function __construct();
    public function getById(int $id);
    public function save(Wallet $wallet);
}