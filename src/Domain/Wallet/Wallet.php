<?php

namespace App\Domain\Wallet;

class Wallet
{
    /**
     * [$user_id description]
     * @var [type]
     */
    private $user_id;

    /**
     * [$wallet_id description]
     * @var [type]
     */
    private $wallet_id;

    /**
     * [$birthdate description]
     * @var [type]
     */
    private $birthdate;

    /**
     * [$document description]
     * @var [type]
     */
    private $document;

    /**
     * [$status description]
     * @var [type]
     */
    private $status;

    public function handleCreate();
    public function handleUpdate();
    public function handleSuspend();
}
