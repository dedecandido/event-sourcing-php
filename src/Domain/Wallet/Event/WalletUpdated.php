<?php

namespace App\Domain\Wallet\Event;

class WalletUpdated extends WalletEvent
{
    /**
     * [$user_id description]
     * @var [type]
     */
    private $user_id;

    /**
     * [$wallet_id description]
     * @var [type]
     */
    private $wallet_id;

    /**
     * [$birthdate description]
     * @var [type]
     */
    private $birthdate;

    /**
     * [$document description]
     * @var [type]
     */
    private $document;

    /**
     * [$status description]
     * @var [type]
     */
    private $status;
}
