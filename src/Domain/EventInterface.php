<?php

namespace App\Domain;

interface EventInterface
{
    /**
     * [serialize description]
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    public function serialize($data) : String;

    /**
     * [unserialize description]
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    public function unserialize($data) : Array;

    /**
     * [process description]
     * @param  DomainEvent $event [description]
     * @return [type]             [description]
     */
    public function process(DomainEvent $event);
}