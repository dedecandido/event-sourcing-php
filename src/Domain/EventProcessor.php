<?php

namespace App\Domain;

class EventProcessor
{
    /**
     * [process description]
     * @param  DomainEvent $event [description]
     * @return [type]             [description]
     */
    public function process(DomainEvent $event)
    {
        $event->process();
    }
}