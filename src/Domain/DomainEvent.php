<?php

namespace App\Domain;

abstract class DomainEvent implements EventInterface
{
    /**
     * [$id description]
     * @var [type]
     */
    private $id;

    /**
     * [$recorded description]
     * @var [type]
     */
    private $recorded;

    /**
     * [$ocurred description]
     * @var [type]
     */
    private $ocurred;

    /**
     * [__construct description]
     */
    public function __construct($ocurred)
    {
        $this->ocurred = $ocurred
    }

    /**
     * [getId description]
     * @return [type] [description]
     */
    public function getId()
    {
        return $this->id;
    }

    public function serialize();
    public function unserialize();
    public function process();
}
